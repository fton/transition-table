#![doc = include_str!("../README.md")]
#![no_std]
extern crate alloc;

use alloc::vec::Vec;

/// Type alias for the transition table entry.
pub type Transition<K, J, OpV> = (K, J, J, OpV);

/// To represent empty trie entry.
/// Provided implementations for integer primitive types as `-1` represents None.
pub trait Optional: Copy {
    type Inner: Copy;

    fn none() -> Self;
    fn some(v: Self::Inner) -> Self;
    fn is_none(&self) -> bool;
    fn is_some(&self) -> bool { !self.is_none() }
    fn inner(&self) -> Option<Self::Inner>;
}

macro_rules! optional_uint {
    ($($ty:ty),+) => {
        $(
            impl Optional for $ty {
                type Inner = $ty;

                fn none() -> Self { !0 }

                fn some(v: Self::Inner) -> Self {
                    assert_ne!(v, Self::none());
                    v
                }

                fn is_none(&self) -> bool {
                    *self == Self::none()
                }

                fn inner(&self) -> Option<Self::Inner> {
                    if self.is_none() {
                        None
                    } else {
                        Some(*self)
                    }
                }
            }
        )+
    };
}

optional_uint! { u8, u16, u32, u64, u128, usize }

impl<T: Copy> Optional for Option<T> {
    type Inner = T;

    fn none() -> Self { None }
    fn some(v: Self::Inner) -> Self { Some(v) }
    fn is_none(&self) -> bool {
        self.is_none()
    }

    fn inner(&self) -> Option<Self::Inner> {
        *self
    }
}

/// Trie generator.
///
/// ```
/// # use transition_table::*;
/// const KEYWORDS: [(&'static str, i8); 3] = [
///     ("A", 1),
///     ("BB", 2),
///     ("BBC", 3),
/// ];
///
/// let tree = Entry::<char, _>::new(KEYWORDS.iter());
/// let tbl: Vec<Transition<_, _, _>> = tree.into();
/// let mut it = tbl.iter();
///
/// assert_eq!(it.next().unwrap(), &('C', 0usize, 0usize, 2usize));
/// assert_eq!(it.next().unwrap(), &('B', 0usize, 1usize, 1usize));
/// assert_eq!(it.next().unwrap(), &('A', 0usize, 0usize, 0usize));
/// assert_eq!(it.next().unwrap(), &('B', 1usize, 2usize, !0usize));
/// assert_eq!(it.next().unwrap(), &('\u{0}', 2usize, 4usize, !0usize));
/// assert!(it.next().is_none());
/// ```
pub struct Entry<K, V> {
    key: K,
    nexts: Vec<Self>,
    value: V,
}

macro_rules! impl_entry_new {
    ($key:ty, $iter:ident) => {
        impl Entry<$key, usize> {
            /// Reading const array, then creating tree structure to generate trie.
            pub fn new<'a, I, S, V>(src: I) -> Self
            where
                I: Iterator<Item = &'a (S, V)>,
                S: 'a + AsRef<str>,
                V: 'a,
            {
                src.enumerate().fold(
                    Self::default(),
                    |mut root, (i, (ref s, _))| {
                        root.push(s.as_ref().$iter(), i);
                        root
                    }
                )
            }
        }
    };
}

impl_entry_new! { char, chars }
impl_entry_new! { u8, bytes }

impl<K, V> Entry<K, V>
where
    K: Copy + Ord,
    V: Optional,
{
    /// Pushing a single entry.
    pub fn push<I>(&mut self, key: I, v: V::Inner)
    where
        I: IntoIterator<Item = K>
    {
        let mut it = key.into_iter();

        match it.next() {
            Some(c) => {
                let i = match self.nexts.binary_search_by_key(&c, |e| e.key) {
                    Ok(i) => i,
                    Err(i) => {
                        self.nexts.insert(i, Self {
                            key: c,
                            nexts: Vec::new(),
                            value: V::none(),
                        });
                        i
                    },
                };

                self.nexts[i].push(it, v)
            },
            None => self.value = V::some(v),
        }
    }

    /// Generating trie.
    pub fn push_to(&self, tbl: &mut Vec<Transition<K, usize, V>>) -> Transition<K, usize, V> {
        let mut v = self.nexts.iter().fold(
            Vec::new(),
            |mut v, child| {
                v.push(child.push_to(tbl));
                v
            }
        );
        let retval = (self.key, tbl.len(), tbl.len() + self.nexts.len(), self.value.clone());

        tbl.append(&mut v);
        retval
    }
}

impl<K, V> Default for Entry<K, V>
where
    K: Default,
    V: Optional,
{
    fn default() -> Self {
        Self {
            key: Default::default(),
            nexts: Vec::new(),
            value: V::none(),
        }
    }
}

impl<K, V> Into<Vec<Transition<K, usize, V>>> for Entry<K, V>
where
    K: Copy + Ord,
    V: Optional,
{
    fn into(self) -> Vec<Transition<K, usize, V>> {
        let mut tbl = Vec::new();
        let root = self.push_to(&mut tbl);

        tbl.push(root);
        tbl
    }
}