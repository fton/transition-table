[![pipeline status](https://gitlab.com/fton/transition-table/badges/main/pipeline.svg)](https://gitlab.com/fton/transition-table/-/commits/main)
[![coverage report](https://gitlab.com/fton/transition-table/badges/main/coverage.svg)](https://gitlab.com/fton/transition-table/-/commits/main)
[![Latest Release](https://gitlab.com/fton/transition-table/-/badges/release.svg)](https://gitlab.com/fton/transition-table/-/releases)
# Description
This package provides the definition and utilities for the trie for the const array with `&str` key item.
# Usage
```
use ::transition_table::*;

const KEYWORDS: [(&'static str, i8); 3] = [
    ("A", 1),
    ("BB", 2),
    ("BBC", 3),
];

let tree = Entry::<char, _>::new(KEYWORDS.iter());
let tbl: Vec<Transition<_, _, _>> = tree.into();
let mut it = tbl.iter();

assert_eq!(it.next().unwrap(), &('C', 0usize, 0usize, 2usize));
assert_eq!(it.next().unwrap(), &('B', 0usize, 1usize, 1usize));
assert_eq!(it.next().unwrap(), &('A', 0usize, 0usize, 0usize));
assert_eq!(it.next().unwrap(), &('B', 1usize, 2usize, !0usize));
assert_eq!(it.next().unwrap(), &('\u{0}', 2usize, 4usize, !0usize));
assert!(it.next().is_none());
```
# Project status
This package is in the early stage.
